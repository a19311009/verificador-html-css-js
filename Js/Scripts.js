if (window.addEventListener) {
    var codigo = "";
    window.addEventListener("keydown", function (e) {
        codigo += String.fromCharCode(e.keyCode);
        if (e.keyCode == 13) {
            buscar(codigo);
            codigo = "";
        }
    }, true);
}

function buscar(codigo) {
    var request = new XMLHttpRequest(request);
    request.open("GET", "http://localhost/api/index2.php?codigo=" + codigo, true);
    request.responseType = 'json';
    request.send();
    request.onload = () => {
        console.log(request.response.Status);
        console.log(request.response.Nombre);
        console.log(request.response.Precio);
        console.log(request.response.Imagen);

        if (request.response.Status == 200) {
            const LabelName = document.getElementById('LblNombre');
            const LabelPrecio = document.getElementById('LblPrecio');
            const ImgProducto = document.getElementById('img');
            LabelName.textContent = request.response.Nombre;
            LabelPrecio.textContent = request.response.Precio;
            ImgProducto.src = "./Img/Productos/" + request.response.Imagen;
        }
    };
}